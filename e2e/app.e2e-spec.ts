import { AngularKomentarzePage } from './app.po';

describe('angular-komentarze App', function() {
  let page: AngularKomentarzePage;

  beforeEach(() => {
    page = new AngularKomentarzePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
